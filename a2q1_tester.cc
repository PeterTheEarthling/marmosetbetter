#include <iostream>
#include <string>
#include <fstream>
#include <cstring>
#include <vector>

using namespace std;

/**
 * This will run a command on the command line and then return what was outputted from that command
 * @param cmd
 * @return
 */
string GetStdoutFromCommand(string cmd) {
    string data;
    FILE * stream;
    const int max_buffer = 256;
    char buffer[max_buffer];
    cmd.append(" 2>&1");

    stream = popen(cmd.c_str(), "r");
    if (stream) {
        while (!feof(stream))
            if (fgets(buffer, max_buffer, stream) != nullptr) data.append(buffer);
        pclose(stream);
    }
    return data;
}

/**
 * Opens two files and compare's there contents
 * @param programFileName name of the program's output you would like to compare to the key's
 * @param keyFileName name of the key you would like to compare it to
 */
bool compareProgramOutput(const string& programFileName, const string& keyFileName) {
    //////////////////READING PROGRAM'S OUTPUT//////////////////////
    //this will read the file with the program's output
    fstream programOutputStream;
    programOutputStream.open(programFileName);
    //we will put the program's output into a vector of strings
    string programOutputLine;
    vector<string> programOutputLines;
    while(getline(programOutputStream, programOutputLine)) {
        programOutputLines.push_back(programOutputLine);
    }
    ////////////////////////////////////////////////////////////////

    //////////////////////READING KEY'S OUTPUT/////////////////////
    //this will read the file with the test key's output
    fstream keyOutputStream;
    keyOutputStream.open(keyFileName);
    //we will put the keys's contents into a vector of strings
    string currentKeyLine;
    vector<string> keyLines;
    while(getline(keyOutputStream, currentKeyLine)) {
        keyLines.push_back(currentKeyLine);
    }
    ////////////////////////////////////////////////////////////////


    bool failedTest = false;
    for(int i = 0; i < keyLines.size(); i ++){
        string keyLine = keyLines[i];
        //FOR SOME REASON ON THE KEY THERE ARE CAIRRAGE RETURN CHARACTERS AT THE END
        //SO WE NEED TO REMOVE THEM OMG
        keyLine = keyLine.substr(0, keyLine.length()-1);
        programOutputLine = programOutputLines[i];

        if(strcmp(keyLine.c_str(), programOutputLine.c_str()) != 0) {
            cout << "ERROR!" << endl;
            cout << (string)"Expected output: "  + keyLine  << endl;
            cout << (string) "Actual output: " + programOutputLine << endl << endl;
            failedTest = true;
        }
    }
    if(keyLines.size() > programOutputLines.size()) {
        failedTest = true;
        cout << "The program's output was shorter than the key's" << endl;
        for(int i = programOutputLines.size(); i < keyLines.size(); i ++) {
            cout << "NEED THIS LINE: " + programOutputLines.at(i) << endl;
        }
    }
    if(keyLines.size() < programOutputLines.size()) {
        failedTest = true;
        cout << "The program's output was longer than the key's" << endl;
    }

    return !failedTest;
}

/**
 * Runs a unix command on the command line
 * @param commandName the command you wish to run
 */
void runCommand(const string& commandName) {
    system(commandName.c_str());
}

/**
 * Call this to actually run the program
 * @param programName name of the executable to run
 * @param inputFileName name of the input file to give the program (relative to the MarmosetBetter folder)
 * @param outputFileName name of the output file to be created with the program's output
 */
void runProgram(const string& programName, const string& programPath,
        const string& inputFileName, const string& outputFileName) {
    //first we need to move the readme_file1 into the current directory
    runCommand((string) "./" + programName + " " + " < " + programPath
          + inputFileName + " > " + outputFileName);
}

/**
 * Runs a series of tests on the a2q1 question
 * @param fileName
 * @param programPath this is the path to the actual binary location on the file system
 */
void test_a2q1(string& fileName, string& programPath) {
    //this will be the name of the program when compiled
    string compiledProgramName = "myProgram";
    //we will now compile the program
    //we will compile with g++
    runCommand("g++ -o " + compiledProgramName + " " + fileName);

    int totalTests = 1;
    //the amount of sadness
    int numberOfFailedTests = 0;

    /////////////////////RUN ALL THE TESTS!!!!!//////////////
    for(int i = 1; i <= totalTests; i ++) {
        string inputFile = "TestFiles/a2q1/input_cin" + to_string(i);
        //now we may run the program
        runProgram(compiledProgramName, programPath, inputFile,"programOutput");
        //compare the program's output to the key
        bool passedTest = compareProgramOutput((string) "programOutput",
                                                programPath + "TestFiles/a2q1/output_key1");
        numberOfFailedTests += !passedTest;
    }
    /////////////////////////////////////////////////////////


    if(numberOfFailedTests == 0) {
        cout << "CONGRATULATIONS! YOU PASSED ALL THE TESTS" << endl;
    }else {
        cout << "YOU FAILED " + to_string(numberOfFailedTests) + " TEST CASE" + (numberOfFailedTests == 1 ? "" : "S") << endl;
    }

    runCommand("rm " + compiledProgramName);
    runCommand("rm programOutput");
}