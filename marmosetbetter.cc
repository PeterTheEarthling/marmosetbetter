#include <iostream>
#include "a2q1_tester.cc"

using namespace std;

int main(int argc, char* argv[]) {
    if(argc <= 1) {
        cerr << "Please specify program file name." << endl;
        return 1;
    }

    //the user will enter the name of the program they would like to test
    //as the second argument of the command line
    string fileName = argv[1];



    //assignments are named like this: a1p2
    //so the second character is the assignment number
    int assignmentNumber = stoi(fileName.substr(1,1));
    //the question number is the 4th character
    int questionNumber = stoi(fileName.substr(3,1));

    //argv holds the path to the program's executable
    string pathToExecutable = argv[0];
    //to get the path to the directory, we need to chop off the marmosetbetter part, which is the executable name
    string pathToDir = pathToExecutable.substr(0, pathToExecutable.length()
         - ((string) "marmosetbetter").length());



    if(assignmentNumber == 2) {
        test_a2q1(fileName, pathToDir);
    }


    return 0;
}